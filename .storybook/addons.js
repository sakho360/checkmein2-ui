 import '@storybook/addon-actions/register';
import '@storybook/addon-links/register';
// import '@storybook/addon-notes/register';
import 'storybook-addon-a11y/register';
// import 'storybook-readme/register';
// import 'storybook-chrome-screenshot/register';
import * as CodeAddon from 'storybook-addon-code/dist/register';
CodeAddon.setTabs([
        { label: 'css', type: 'sass' },
]);