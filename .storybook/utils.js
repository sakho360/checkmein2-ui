import * as React from 'react';
import { ThemeProvider } from 'styled-components';



export const RenderWithTheme = (Component) => {

    const theme = {
        colors: {
            mainBlue:  '#00BCD4',    
            darkGrey: '#626369',
            lightGrey: '#D3D3D3',
            white:  '#ffffff'
        }

    };

    return (
        <ThemeProvider theme={theme}>
            {Component}
        </ThemeProvider>
    );
};