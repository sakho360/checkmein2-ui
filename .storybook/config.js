import { configure } from '@storybook/react';

function loadStories() {
  require('../src/.stories/AppStory.js');
  require('../src/.stories/DeadLineSelectorStory.js');
  require('../src/.stories/FileUploaderStory.js');
  require('../src/.stories/FooterStory.js');
  require('../src/.stories/HeaderStory.js');
  require('../src/.stories/NotFoundStory.js');
  require('../src/.stories/TextEditorStory.js');
  require('../src/.stories/campaigns/AddCampaignStory.js');
  require('../src/.stories/campaigns/CampaignCardStory.js');
  require('../src/.stories/campaigns/CampaignsStory.js');
}

configure(loadStories, module);
