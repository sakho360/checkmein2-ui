import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import renderer from 'react-test-renderer';
import NotFound from '../components/NotFound';

const getComponent = props => 
    <NotFound 
        className='first'
        onClick={jest.fn()}
        {...props}
    />

describe('NotFound component', () => {
    it('renders without crashing', () => {
        const tree = renderer.create(getComponent()).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('should render with props', () => {
        const tree = renderer.create(getComponent({
            className: 'second'
        })).toJSON();
        expect(tree).toMatchSnapshot();     
    });
    it('should render with class passed in props', () => {
        const wrapper = shallow(getComponent({
            className: 'third'
        }));
        expect(wrapper.find('.third').length).toBe(1);
    });
    it('should call onClick when component is clicked', () => {
        const spyOnClick = jest.fn();
        const wrapper = shallow(getComponent({
            onClick: spyOnClick
        }));
        wrapper.simulate('click');
        expect(spyOnClick).toHaveBeenCalled();
    });
});
