import { graphql } from 'react-apollo';
import { createCampaignMutation } from '../../utils/Mutations';
import AddCampaign from '../../components/campaigns/AddCampaign';

export default graphql(createCampaignMutation, {
    props: ({ mutate }) => ({
        saveCampaign: (name, content, user_id, startDate, endDate, summary,
            deadline_type, is_launched, deadline_run_for) =>
            mutate({
                variables: {
                    name,
                    content,
                    user_id, // update micro service to take user_id from token
                    startDate,
                    endDate,
                    summary,
                    deadline_type,
                    is_launched,
                    deadline_run_for
                }
            }).then(resp => {
                document.location = '/campaigns';
            })
    })
})(AddCampaign);
