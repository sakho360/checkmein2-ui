import gql from 'graphql-tag';

export const allCampaignsQuery = gql`
  query allcampaigns {
    campaigns {
      _id,
      name,
      content,
      user_id,
      startDate,
      endDate
    }
  }
`;

export const currentUserQuery = gql`
  query currentUserData {
    currentUser {
      name,
      email,
      id,
      user_role
    }
  }
`;
export const campaingByIdQuery = gql`
 query currentCampaign($campaignId: ID!) {
    campaignById(campaign_id: $campaignId) {
      name,
      content,
      user_id,
      startDate,
      endDate,
      media_id
    }
  }
`;

export const challengeByIdQuery = gql`
  query challengeById($challengeId: ID!) {
    challengeById(challenge_id: $challengeId) {
      _id,
      campaign_id,
      owner_id,
      name,
      description,
      media_id,
      checkin_types {
        photo,
        numeric,
        lat,
        lng
      },
      sponsors {
        user_id
        description
      },
      participants {
        user_id
      }
    }
  }
`;

export const challengesForCampaignQuery = gql`
 query allChalleanges($campaignId: ID!) {
    challengesForCampaign(campaign_id: $campaignId){
      _id,
      campaign_id,
      owner_id,
      name,
      description,
      media_id,
      checkin_types {
        photo,
        numeric,
        lat,
        lng
      },
      sponsors {
        user_id
        description
      },
      participants {
        user_id
      }
    }
  }
`;
