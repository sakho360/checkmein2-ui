import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import NotFound from '../components/NotFound';

storiesOf('Not Found', module)
  .add('standard', () => (
    <NotFound />
  ))