import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import { DeadLineSelector } from '../components/DeadLineSelector';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { RadioButton } from 'material-ui/RadioButton';
import * as Utils from '../../.storybook/utils'
storiesOf('DeadLineSelector', module)
  .add('basic with no deadline selected', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <DeadLineSelector 
          onDeadLineSelect={action('Dead line select')}
          handleDeadLineDateChange={action('date change')}
          selectedDeadline={1}
          handleInputChange={action('input change')}
          onInputClick={action('input click')} />
      </MuiThemeProvider>
    )
  ))
  .add('basic with Run for selected', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <DeadLineSelector 
          onDeadLineSelect={action('Dead line select')}
          handleDeadLineDateChange={action('date change')}
          selectedDeadline={2}
          handleInputChange={action('input change')}
          onInputClick={action('input click')} />
      </MuiThemeProvider>
    )
  ))
  .add('basic with Run until selected', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <DeadLineSelector 
          onDeadLineSelect={action('Dead line select')}
          handleDeadLineDateChange={action('date change')}
          selectedDeadline={3}
          handleInputChange={action('input change')}
          onInputClick={action('input click')} />
      </MuiThemeProvider>
    )
  ));



