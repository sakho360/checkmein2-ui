import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import {CampaignCard} from '../../components/campaigns/CampaignCard';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Utils from '../../../.storybook/utils'
import { Flex, Box } from 'grid-styled';

const data = {
  name: 'Test',
  content: "<p>This is a paragraph</p>",
  startDate: 12341,
  endDate: 2134,
  pictureUrl: 'https://ksr-ugc.imgix.net/assets/018/017/062/3639b75dd430d036764962d53024a36c_original.png?crop=faces&w=560&h=315&fit=crop&v=1503922815&auto=format&q=92&s=a12a9fd456337c2c1ac71782113ab05f'
};

storiesOf('Campaign Card', module)
  .add('with one card', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <Flex >
          <Box>
              <CampaignCard 
                  {...data}
              />
          </Box>
        </Flex>
      </MuiThemeProvider>
    )
  ))
  .add('with multiple cards', () => (
    Utils.RenderWithTheme(
      <MuiThemeProvider>
        <Flex >
          <Box width={[1, 1/2, 1/3]} px={2} py={3}> 
              <CampaignCard 
                {...data}
              />
          </Box>
          <Box width={[1, 1/2, 1/3]} px={2} py={3}>          
              <CampaignCard 
                {...data}
              />
          </Box>
          <Box width={[1, 1/2, 1/3]} px={2} py={3}>          
              <CampaignCard 
                {...data}
              />
          </Box>
        </Flex>
      </MuiThemeProvider>
    )
  ));;