import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import Header from '../components/Header'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import * as Utils from '../../.storybook/utils'

storiesOf('Header', module)
  .add('standard', () => (
      <MuiThemeProvider>
        <Header />
      </MuiThemeProvider>
  ))