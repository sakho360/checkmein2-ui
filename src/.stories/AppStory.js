import { storiesOf } from '@storybook/react';
import * as React from 'react';
import App from '../components/App'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import StoryRouter from 'storybook-router';

storiesOf('Main App', module)
  .addDecorator(StoryRouter())
  .add('standard', () => (
    <MuiThemeProvider>  
        <App />
    </MuiThemeProvider>
  ))