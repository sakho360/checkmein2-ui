import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import * as React from 'react';
import { TextEditor } from '../components/TextEditor'
import 'react-quill/dist/quill.snow.css';

const style = {
  width: '60%',
  height: '150px'
}

  storiesOf('Text Editor', module)
  .add('texteditor', () => (
      <div style={style}> 
        <TextEditor
          placeholder={'Placeholder text'}
          onChange={action('text-editor')}
          height={'100px'} />
      </div>
    ));