import React from 'react';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';

const FileUploaderContent = styled.div`
    display: flex;
    text-align: left;
    padding: 20px;
    margin: 10px;
    border: 0.5px solid ${props => props.theme.colors.lightGrey};
`;

const FileUploaderTitle = styled.div`
    width: 40%;
`;

const ButtonContainer = styled.div`
    width: 30%;
    position: relative;
    display: inline-block;
`;

const FileUploadButton = styled.div`
    position: absolute;
    bottom: 40%;
    left: 25%;
`;

const BackgroundImage = styled.img`
    width: 100%;
    height: 100%;
`;

const ImageUploader = styled.input`
    display: none;
`;

export const FileUploader = withTheme(({ title,description,defaultPicture,buttonLabel }) =>
    <FileUploaderContent>
        <FileUploaderTitle>
            <div>
                <strong>{title}</strong>
            </div>
            <div>
                <label>{description}</label>
            </div>
        </FileUploaderTitle>
        <ButtonContainer>
            <BackgroundImage alt="Image Upload" src={ defaultPicture } />
            <FileUploadButton>
                <RaisedButton 
                    label={buttonLabel} 
                    containerElement='label'
                    primary>
                    <ImageUploader type="file" />
                </RaisedButton>
            </FileUploadButton>
        </ButtonContainer>
    </FileUploaderContent>)

FileUploader.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    defaultPicture: PropTypes.string.isRequired,
    buttonLabel: PropTypes.string.isRequired
};