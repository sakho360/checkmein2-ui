import React from 'react';
import logo from './logo.svg';
import FacebookLoginButton from './FacebookLoginButton';
import styled from 'styled-components';

const AppLogo = styled.img`
    @keyframes App-logo-spin {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }
    animation: App-logo-spin infinite 20s linear;
    height: 80px;
`;

export default () =>
    <div className="App-header">
        <AppLogo src={logo} alt="logo" />
        <h2>Welcome to CheckMeIn2</h2>
        <FacebookLoginButton />
    </div>;
