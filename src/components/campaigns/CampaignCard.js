import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import { Card, CardActions, CardText } from 'material-ui/Card';
import styled from 'styled-components';
import colors from '../../utils/Colors';

const Category = styled.div`
    background-color: ${colors.mainBlue}; 
    position: absolute;
    width: auto;
    height: 20px;
    padding: 5px;
    color: ${colors.white};
    border-radius: 2px;
    margin-left: 20px;
    margin-top: -15px;
    z-index: 1;
    font-family: ${props => props.theme.fontFamily};
`;

const HeaderImage = styled.img`
    width: 100%;
    height: 100%;
`;

const Title = styled.div`
    color: ${colors.darkGrey};
    text-align: left;
    font-size: 15px;
    padding-bottom: 10px;
`;

const CardContent = styled.div`
    text-align: left;
    padding: 5px;
    font-family: ${props => props.theme.fontFamily}; 
`;

const ButtonStyle = styled.div`
    color: ${colors.white};
    font-family: ${props => props.theme.fontFamily};
`;

const ProgressBarContainer = styled.div`
    background-color: ${colors.darkGrey};
    width: auto;
    height: 5px;
    margin: 0px 20px;
    border-radius: 2px;
`;

const ProgressBarFill = styled.div`
    background-color: ${colors.mainBlue};
    height: 100%;
`;

const ContentDiv = styled.div`
    padding-bottom: 10px;
`;

const getDateString = (date) => (new Date(date)).toDateString();

const ProgressBar = ({ startDate, endDate }) => {
    const dateInterval = (endDate - startDate);
    const currentDate = new Date().getTime();
    const intervalLeft = endDate - currentDate;
    let filledPercent = 100;
    if (startDate > currentDate)
        filledPercent = 0;
    else if (intervalLeft > 0)
        filledPercent = Math.round(((dateInterval - intervalLeft) / dateInterval) * 100);

    return (
        <ProgressBarContainer>
            <ProgressBarFill style={{ "width": `${filledPercent}%` }} />
        </ProgressBarContainer>
    );
};

export const CampaignCard = ({ name, content, startDate, endDate, pictureUrl }) =>
    <Card>
        <HeaderImage alt="Header Image" src={ pictureUrl } />        
        <Category>
            Projects we love
        </Category>
        <CardText>
            <CardContent>
                <Title>{name}</Title>
                <ContentDiv dangerouslySetInnerHTML={{ __html: content }} /> 
                <div>Created By: username </div>
                <div>Start Date: { getDateString(startDate) }</div>
                <div>End Date: { getDateString(endDate) }</div>
            </CardContent>
        </CardText>
        <ProgressBar startDate={startDate} endDate={endDate} />
        <CardActions>
            <FlatButton backgroundColor={colors.mainBlue}>
                <ButtonStyle>Delete</ButtonStyle>
            </FlatButton>
        </CardActions>
    </Card>