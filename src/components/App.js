import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from '../services/AuthService';
import Header from './Header';
import { Footer } from './Footer';
import NotFound from './NotFound';
import CampaignsContainer from '../containers/campaigns/CampaignsContainer';
import AddCampaignContainer from '../containers/campaigns/AddCampaignContainer';
import Login from './login/LoginComponent';
import styled from 'styled-components';
import { ThemeProvider } from 'styled-components';
import colors from '../utils/Colors';

const theme = {
    colors: Object.assign(colors),
    fontFamily: "Comic Sans MS"
}

const PrivateRoute = ({ component: Component, ...rest }) =>
    <Route
        {...rest}
        render={props =>
            isAuthenticated()
                ? <Component {...props} />
                : <Redirect
                      to={{
                          pathname: '/login',
                          state: { from: props.location }
                      }}
                    />}
    />;

const AppWrapper = styled.div`
    text-align: center;
`;

const Content = styled.div`
    margin: auto;
    width: 70%;
`;

const App = () =>
 <ThemeProvider theme={theme}>
    <AppWrapper>       
        <Header />        
        <Content>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/404" component={NotFound} />
                <PrivateRoute path="/campaigns" component={CampaignsContainer} />
                <PrivateRoute 
                    path="/add-campaigns"
                    component={AddCampaignContainer}/>
            </Switch>
        </Content>        
        <Footer />        
    </AppWrapper>
</ThemeProvider>

export default App;
