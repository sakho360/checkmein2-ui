import React from 'react';
import styled from 'styled-components';
import { RadioButton } from 'material-ui/RadioButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import { deadLineOptions } from '../utils/Constants';

const RadioElement = styled.div`
    display: flex;
    flex-wrap: wrap-reverse;
`;

const DeadLineContent = styled.div`
    display: flex;
    text-align: left;
    padding: 20px;
    margin: 10px;
    border: 0.5px solid ${props => props.theme.colors.lightGrey};
`;

const DeadLineTitle = styled.div`
    width: 40%;
`;

const DeadLineOptions = styled.div`
    width: 60%;
`;

export const DeadLineSelector = (props) =>
    <DeadLineContent>
        <DeadLineTitle>
            <div>
                <strong>Deadline (recommended)</strong>
            </div>
            <div>
                <label>Helps you raise more money, faster.</label>
            </div>
        </DeadLineTitle>
        <DeadLineOptions>
            <RadioButton 
                onClick={props.onDeadLineSelect}
                value={deadLineOptions.NO_DEADLINE} 
                label="No deadline"
                checked={props.selectedDeadline === deadLineOptions.NO_DEADLINE}/>
            <RadioElement>
                <RadioButton
                    onClick={props.onDeadLineSelect}
                    value={deadLineOptions.RUN_FOR} 
                    label="Run for..." 
                    checked={props.selectedDeadline === deadLineOptions.RUN_FOR}
                    style={{ "width": "20%" }}
                    className="js-radio-button-2"/>
                <TextField 
                    id={deadLineOptions.RUN_FOR}
                    onChange={props.handleInputChange} 
                    name="deadlineInput"
                    onClick={props.onInputClick}
                    className="js-textfield-button" />
                <p>days left</p>
            </RadioElement>
            <RadioElement>
                <RadioButton
                    onClick={props.onDeadLineSelect}
                    value={deadLineOptions.RUN_UNTIL}
                    label="Run until..." 
                    checked={props.selectedDeadline === deadLineOptions.RUN_UNTIL} 
                    style={{ "width": "20%" }} />
                <DatePicker
                    id={deadLineOptions.RUN_UNTIL}
                    name="deadLineDate"
                    floatingLabelText="Pick a Date"
                    onChange={props.handleDeadLineDateChange}
                    onClick={props.onInputClick}
                    className="js-datepicker-button" />
            </RadioElement>
        </DeadLineOptions>
    </DeadLineContent>